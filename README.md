# archi_and_infra_terraform

## Platform 2

Create a terraform.tfvars with the following variables:
* vpc_cidr           = "10.11.0.0/16"
* public_subnet_cidr = "10.11.1.0/24"
* aws_access_key = "XXXXXXXXXXXXX"
* aws_secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
* aws_region     = "eu-west-1"
* vm_instance_name               = "myvm"
* vm_instance_type               = "t3.small"
* vm_associate_public_ip_address = true
* vm_root_volume_size            = 30
* vm_root_volume_type            = "gp2"
* vm_data_volume_size            = 10
* vm_data_volume_type            = "gp2"
