output "S3_bucket_url" {
  value = "front-end : ${aws_s3_bucket.front-end.bucket_domain_name}"
}

output "cloundfront_url" {
  value = "front-end-hosted : ${aws_cloudfront_distribution.front-end.domain_name}"
}

