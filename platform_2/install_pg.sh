#!/bin/bash

sudo apt-get update
sudo apt-get install postgresql postgresql-contrib -y
# enable service
sudo systemctl enable postgresql
# start service
sudo systemctl start postgresql


# create user, its password and database
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres';"
sudo -u postgres psql -c "CREATE DATABASE postgres;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE postgres TO postgres;"

sudo echo "# Database administrative login by Unix domain socket
local   all             postgres                                trust

# TYPE  DATABASE        USER            ADDRESS                 METHOD

# \"local\" is for Unix domain socket connections only
local   all             all                                     md5
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
host    all             all             0.0.0.0/0            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
# Allow replication connections from localhost, by a user with the
# replication privilege.
local   replication     all                                     peer
host    replication     all             127.0.0.1/32            md5
host    replication     all             ::1/128                 md5
" > /etc/postgresql/13/main/pg_hba.conf

sudo echo "listen_addresses = '*'" >> /etc/postgresql/13/main/postgresql.conf


sudo systemctl restart postgresql
