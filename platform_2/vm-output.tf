output "vm_server_instance_public_ip" {
  ## print the public ip of the api server and db server
  value = "db_server_public_ip : : ${aws_instance.db-server.public_ip}\napi_server_public_ip :   ${aws_instance.api-server.public_ip} front_server_public_ip : ${aws_instance.front-end.public_ip}"
}

output "vm_server_instance_private_ip" {
  value = "db_server_private_ip : ${aws_instance.db-server.private_ip}\napi_server_private_ip :  ${aws_instance.api-server.private_ip}"
}

