variable "vm_instance_type" {
  type        = string
  description = "EC2 instance type"
  default     = "t3.small"
}

variable "vm_associate_public_ip_address" {
  type        = bool
  description = "Associate a public IP address to the EC2 instance"
  default     = true
}

variable "vm_root_volume_size" {
  type        = number
  description = "Root Volume size of the EC2 Instance"
}

variable "vm_data_volume_size" {
  type        = number
  description = "Data volume size of the EC2 Instance"
}

variable "vm_root_volume_type" {
  type        = string
  description = "Root volume type of the EC2 Instance"
  default     = "gp2"
}

variable "vm_data_volume_type" {
  type        = string
  description = "Data volume type of the EC2 Instance"
  default     = "gp2"
}

variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "aws_region" {
  type        = string
  description = "AWS Region"
  default     = "eu-west-1"
}

variable "vm_instance_name" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "linux-server-vm"
}
