# publish the front end to S3 using aws_s3_bucket_website_configuration
resource "aws_s3_bucket_website_configuration" "aws_s3_bucket_website" {
  bucket = aws_s3_bucket.front-end.id

  index_document {
    suffix = "index.html"
  }

  routing_rule {
    redirect {
      replace_key_with = "index.html"
    }
  }
}

locals {
  content_type_map = {
    css:  "text/css; charset=UTF-8"
    js:   "text/js; charset=UTF-8"
    svg:  "image/png+xml"
  }
}


data "aws_iam_policy_document" "website_policy" {
  statement {
    actions = [
      "s3:GetObject"
    ]
    principals {
      identifiers = ["*"]
      type = "AWS"
    }
    resources = [
      "arn:aws:s3:::${var.domain-name}/*"
    ]
  }
}

# Create the bucket
resource "aws_s3_bucket" "front-end" {
  bucket = var.domain-name
  tags = {
    Name = "front-end"
  }
  policy = data.aws_iam_policy_document.website_policy.json
}


# apply the acl to the bucket
resource "aws_s3_bucket_acl" "front-end" {
  bucket = aws_s3_bucket.front-end.id
  acl =  "public-read"
}

resource aws_s3_object assets {
  for_each = fileset("${path.module}/build", "**")

  bucket = aws_s3_bucket.front-end.id
  key    = each.value
  source = "${path.module}/build/${each.value}"
  etag   = filemd5("${path.module}/build/${each.value}")

  // simplification of the content type serving
  content_type = lookup(
    local.content_type_map,
    split(".", basename(each.value))[length(split(".", basename(each.value))) - 1],
    "text/html; charset=UTF-8",
  )
}

resource "aws_cloudfront_distribution" "front-end" {
  origin {
    domain_name = aws_s3_bucket.front-end.bucket_domain_name
    origin_id   = aws_s3_bucket.front-end.id
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "front-end"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = aws_s3_bucket.front-end.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  tags = {
    Name = "front-end"
  }
}