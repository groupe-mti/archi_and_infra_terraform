# Create the vpc
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
}

# Define the internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id
}


#### PUBLIC SUBNET ####

# Define the public subnet
resource "aws_subnet" "public-subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.public_subnet_cidr
  availability_zone = var.aws_az
  map_public_ip_on_launch = "true"

}


# Define the public route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

# Assign the public route table to the public subnet
resource "aws_route_table_association" "public-rt-association" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id
}

#### PRIVATE SUBNET ####

# Define the private subnet
resource "aws_subnet" "private-subnet" {
    vpc_id            = aws_vpc.vpc.id
    cidr_block        = var.private_subnet_cidr
    availability_zone = var.aws_az
    tags = {
        Name = "prod-subnet-private-1"
    }
}

## aws_security_group for redis
resource "aws_security_group" "redis" {
  name        = "redis"
  description = "Allow redis traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "Redis"
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 
  // lambda
  ingress {
    description = "lambda"
    from_port   = 80
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "redis"
  }
}