#!/bin/bash

sudo apt-get update

# install gcc git and libpq-dev
sudo apt-get install libpq-dev gcc git unzip -y

cd /root

wget https://gitlab.com/groupe-mti/archi_and_infra_back/-/jobs/3610383070/artifacts/download

unzip download

echo "DATABASE_URL=postgres://postgres:postgres@${db_server_ip}:5432" > .env

echo '[global]
address = "0.0.0.0"' >> Rocket.toml

./target/release/archi_and_infra_back > log.log 2>&1
