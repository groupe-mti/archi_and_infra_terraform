terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.aws_region
  ## set key and id
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}


// Elasticache redis
// passer la connection string dans l'env de la lambda
// passer l'url de l'api gateway dans le frosi nt gl hf write l'endpoint


//  include the ec2:AssignPrivateIpAddresses and ec2:UnassignPrivateIpAddresses actions in  permissions


# data "aws_iam_policy_document" "AWSLambdaTrustPolicy" {
#   version = "2012-10-17"
#   statement {
#     actions = [

#     ]
#     resources = ["*"]
#   }
# }



# deploy the lambda function located in ../../archi_and_infra_back_platform_3
resource "aws_apigatewayv2_api" "lambda" {
  name          = "serverless_lambda_gw"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "lambda" {
  api_id = aws_apigatewayv2_api.lambda.id

  name        = "serverless_lambda_stage"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "post_item" {
  api_id = aws_apigatewayv2_api.lambda.id

  integration_uri    = aws_lambda_function.back-end.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "post_item" {
  api_id = aws_apigatewayv2_api.lambda.id

  route_key = "POST /item"
  target    = "integrations/${aws_apigatewayv2_integration.post_item.id}"
}

resource "aws_apigatewayv2_integration" "get_items" {
  api_id = aws_apigatewayv2_api.lambda.id

  integration_uri    = aws_lambda_function.back-end.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "get_items" {
  api_id = aws_apigatewayv2_api.lambda.id

  route_key = "GET /"
  target    = "integrations/${aws_apigatewayv2_integration.get_items.id}"
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.lambda.name}"

  retention_in_days = 30
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.back-end.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.lambda.execution_arn}/*/*"
}










