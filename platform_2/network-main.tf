# Create the vpc
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
}

# Define the internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id
}


#### PUBLIC SUBNET ####

# Define the public subnet
resource "aws_subnet" "public-subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.public_subnet_cidr
  availability_zone = var.aws_az
  map_public_ip_on_launch = "true"

}


# Define the public route table
resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

# Assign the public route table to the public subnet
resource "aws_route_table_association" "public-rt-association" {
  subnet_id      = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-rt.id
}

#### PRIVATE SUBNET ####

# Define the private subnet
resource "aws_subnet" "private-subnet" {
    vpc_id            = aws_vpc.vpc.id
    cidr_block        = var.private_subnet_cidr
    availability_zone = var.aws_az
    tags = {
        Name = "prod-subnet-private-1"
    }
}

# Define the private route table
resource "aws_route_table" "private-rt" {
    vpc_id = aws_vpc.vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = aws_nat_gateway.nat-gw.id
    }
}

# Assign the private route table to the private subnet

resource "aws_route_table_association" "private-rt-association" {
    subnet_id      = aws_subnet.private-subnet.id
    route_table_id = aws_route_table.private-rt.id
}

# EIP for the NAT Gateway
resource "aws_eip" "nat-gw-eip" {
    vpc = true
    tags = {
        Name = "prod-nat-gw-eip"
    }
}

# NAT Gateway to allow private subnet to connect to the internet via the public subnet
resource "aws_nat_gateway" "nat-gw" {
    allocation_id = aws_eip.nat-gw-eip.id
    subnet_id     = aws_subnet.public-subnet.id
    tags = {
        Name = "prod-nat-gw"
    }

    depends_on = [aws_internet_gateway.gw]
}

