terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.aws_region
  ## set key and id
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

# Get Latest Debian 11 Bullseye AMI
data "aws_ami" "debian-11" {
  most_recent = true
  owners      = ["136693071363"]

  filter {
    name   = "name"
    values = ["debian-11-amd64-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# Define the security group for the EC2 Instance
resource "aws_security_group" "aws-vm-sg" {
  name        = "vm-sg"
  description = "Allow incoming connections"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow incoming HTTP connections"
  }

  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow incoming HTTP connections"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow incoming SSH connections (Linux)"
  }

  ## allow postgresql connections from the api server to the db server
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
    description = "Allow incoming postgresql connections"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "my-sg"
  }
}


# Create DB Instance
resource "aws_instance" "db-server" {
  ami                    = data.aws_ami.debian-11.id
  instance_type          = var.vm_instance_type
  subnet_id              = aws_subnet.private-subnet.id
  vpc_security_group_ids = [aws_security_group.aws-vm-sg.id]
  source_dest_check      = false
  key_name               = "aws_key"

  user_data = file("install_pg.sh")

  # root disk
  root_block_device {
    volume_size           = var.vm_root_volume_size
    volume_type           = var.vm_root_volume_type
    delete_on_termination = true
    encrypted             = true
  }

  depends_on = [
    aws_route_table_association.private-rt-association
  ]
  tags = {
    Name = "db-server"
  }
}

# Create API Instance


locals {
  db_server_ip = aws_instance.db-server.private_ip
  depends_on   = [aws_instance.db-server]
}

resource "aws_instance" "api-server" {
  ami                    = data.aws_ami.debian-11.id
  instance_type          = var.vm_instance_type
  subnet_id              = aws_subnet.public-subnet.id
  vpc_security_group_ids = [aws_security_group.aws-vm-sg.id]
  source_dest_check      = false
  key_name               = "aws_key"

  depends_on = [
    aws_instance.db-server
  ]

  ## give the ip of the db server to the api server
  user_data = base64encode(templatefile("install_api.sh", {
    db_server_ip = local.db_server_ip
  }))

  # root disk
  root_block_device {
    volume_size           = var.vm_root_volume_size
    volume_type           = var.vm_root_volume_type
    delete_on_termination = true
    encrypted             = true
  }

  tags = {
    Name = "api-server"
  }
}


# Create the front end instance

resource "aws_instance" "front-end" {
  ami                    = data.aws_ami.debian-11.id
  instance_type          = var.vm_instance_type
  subnet_id              = aws_subnet.public-subnet.id
  vpc_security_group_ids = [aws_security_group.aws-vm-sg.id]
  source_dest_check      = false
  key_name              = "aws_key"
  depends_on   = [aws_instance.api-server]


  // give the api server ip to the front server
  user_data = base64encode(templatefile("install_front.sh", {
    api_server_ip = aws_instance.api-server.public_ip
  }))


   root_block_device {
    volume_size           = var.vm_root_volume_size
    volume_type           = var.vm_root_volume_type
    delete_on_termination = true
    encrypted             = true
  }

  tags = {
    Name = "front-server"
  }
}

