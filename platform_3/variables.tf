variable "aws_region" {
  type        = string
  description = "AWS Region"
  default     = "eu-west-1"
}

variable "domain-name" {
  type        = string
  description = "AWS S3 bucket name"
  default     = "arinf-holoconte-front-end"
}

variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "function_name" {
  default = "back-end"
}

variable "cluster_id" {
  default = "demo-redis-cluster"
}

variable "instance_type" {
  default = "cache.t2.micro"
}

variable "maintenance_window" {
  default = "sun:05:00-sun:06:00"
}

variable "function_handler" {
  default = "index.handler"
}

variable "file_name" {
  default = "demo-lambda-redis.zip"
}


# tags
variable "tag_name" {
  default = "demo-redis-cluster"
}


variable "vm_instance_type" {
  default = "t3.small"
}

variable "engine_version" {
  default = "2.8.24"
}

variable "parameter_group_name" {
  default = "default.redis7"
}

variable "tag_contact-email" {
  default = "example@domain.com"
}

variable "attach_vpc_config" {
  default = true
}




