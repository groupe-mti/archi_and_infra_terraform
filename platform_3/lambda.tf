resource "aws_security_group" "redis_sg" {
  vpc_id = aws_vpc.vpc.id

  ingress {
    cidr_blocks = [var.private_subnet_cidr]
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.tag_name}"
  }
}

# Create ElastiCache Redis subnet group

resource "aws_elasticache_subnet_group" "default" {
  name        = "subnet-group-${var.tag_name}"
  description = "Private subnets for the ElastiCache instances: ${var.tag_name}"
  subnet_ids  = [aws_subnet.private-subnet.id]
}

# Create ElastiCache Redis cluster

resource "aws_elasticache_cluster" "redis" {
  cluster_id           = var.cluster_id
  engine               = "redis"
  maintenance_window   = var.maintenance_window
  node_type            = var.instance_type
  num_cache_nodes      = "1"
  parameter_group_name = var.parameter_group_name
  port                 = "6379"
  subnet_group_name    = aws_elasticache_subnet_group.default.name
  security_group_ids   = ["${aws_security_group.redis_sg.id}"]

  tags = {
    Name = "${var.tag_name}"
  }
}

# Create IAM role for Lambda function

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda-vpc-role" {
  name               = var.function_name
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

# Attach an additional policy to Lambda function IAM role required for the VPC config

data "aws_iam_policy_document" "network" {
  statement {
    effect = "Allow"

    actions = [
      "ec2:CreateNetworkInterface",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "network" {
  name   = "${var.function_name}-network"
  policy = data.aws_iam_policy_document.network.json
}

resource "aws_iam_policy_attachment" "network" {
  count = var.attach_vpc_config ? 1 : 0

  name       = "${var.function_name}-network"
  roles      = ["${aws_iam_role.lambda-vpc-role.name}"]
  policy_arn = aws_iam_policy.network.arn
}

# Create Lambda function

resource "null_resource" "lambda_function" {
  provisioner "local-exec" {
    command = "pwd"
  }
}

resource "aws_lambda_function" "back-end" {
  function_name = var.function_name
  handler       = "index.handler"
  runtime       = "nodejs18.x"
  filename      = "./main.zip"
  role          = aws_iam_role.lambda-vpc-role.arn
  vpc_config {
    subnet_ids         = [aws_subnet.private-subnet.id]
    security_group_ids = ["${aws_security_group.redis_sg.id}"]
  }
  environment {
    variables = { REDIS_URL = "redis://${aws_elasticache_cluster.redis.cache_nodes.0.address}:${aws_elasticache_cluster.redis.cache_nodes.0.port}" }
  }
}
