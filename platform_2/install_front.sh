#!/bin/bash

sudo apt-get update

# install nginx 
sudo apt-get install nginx -y

# enable service
sudo systemctl enable nginx

# wget the repo
wget https://gitlab.com/groupe-mti/archi_and_infra_front/-/archive/main/archi_and_infra_front-main.zip

# install unzip
sudo apt-get install unzip -y

# unzip the repo
unzip archi_and_infra_front-main.zip

# install node 18
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -

# install npm
sudo apt-get install -y nodejs

# install node modules
cd archi_and_infra_front-main
npm install

# create the .env

echo "REACT_APP_API_URL=http://${api_server_ip}:8000" > .env

# build the app
npm run build

# copy the build to the nginx folder
sudo cp -r build/* /var/www/html/

# restart nginx
sudo systemctl restart nginx
